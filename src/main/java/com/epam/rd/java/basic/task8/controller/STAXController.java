package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stax.StAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.epam.rd.java.basic.task8.controller.TagNames.*;
import static com.epam.rd.java.basic.task8.controller.AttributeNames.*;


/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	private List<Flower> flowerList;
	private Flowers objectContainer;
	private Flower currentFlower;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void validate() {
		try {
			XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(new File(xmlFileName.replaceFirst(".xml", ".xsd")));
			Validator validator = schema.newValidator();
			validator.validate(new StAXSource(reader));
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void parse() {
		try {
			XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

			while(reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if(event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					switch(startElement.getName().getLocalPart()) {
						case FLOWERS:
							flowerList = new ArrayList<>();
							break;
						case FLOWER:
							currentFlower = new Flower();
							flowerList.add(currentFlower);
							break;
						case NAME:
							event = reader.nextEvent();
							currentFlower.setName(event.asCharacters().getData());
							break;
						case SOIL:
							event = reader.nextEvent();
							currentFlower.setSoil(event.asCharacters().getData());
							break;
						case ORIGIN:
							event = reader.nextEvent();
							currentFlower.setOrigin(event.asCharacters().getData());
							break;
						case VISUAL_PARAMETERS:
							currentFlower.setVisualParameters(new VisualParameters());
							break;
						case STEM_COLOUR:
							event = reader.nextEvent();
							currentFlower.getVisualParameters().setStemColour(event.asCharacters().getData());
							break;
						case LEAF_COLOUR:
							event = reader.nextEvent();
							currentFlower.getVisualParameters().setLeafColour(event.asCharacters().getData());
							break;
						case AVE_LEN_FLOWER:
							event = reader.nextEvent();
							currentFlower
									.getVisualParameters()
									.setAveLenFlowers(
											new AveLenFlowers(
													Integer.parseInt(event.asCharacters().getData()),
													startElement.getAttributeByName(new QName(MEASURE)).getValue()
											)
									);
							break;
						case GROWING_TIPS:
							currentFlower.setGrowingTips(new GrowingTips());
							break;
						case TEMPERATURE:
							event = reader.nextEvent();
							currentFlower
									.getGrowingTips()
									.setTemperature(
											new Temperature(
													Integer.parseInt(event.asCharacters().getData()),
													startElement.getAttributeByName(new QName(MEASURE)).getValue())
									);
							break;
						case LIGHTING:
							currentFlower
									.getGrowingTips()
									.setLighting(
											new Lighting(
													startElement.getAttributeByName(new QName(LIGHT_REQUIRING)).getValue()
											)
									);
							break;
						case WATERING:
							event = reader.nextEvent();
							currentFlower
									.getGrowingTips()
									.setWatering(
											new Watering(
													Integer.parseInt(event.asCharacters().getData()),
													startElement.getAttributeByName(new QName(MEASURE)).getValue())
									);
							break;
						case MULTIPLYING:
							event = reader.nextEvent();
							currentFlower.setMultiplying(event.asCharacters().getData());
							break;
					}
				}
			}
			objectContainer = new Flowers(flowerList);
		}
		catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void sortObjectContainer(Comparator<Flower> flowerComparator) {
		objectContainer.getFlowers().sort(flowerComparator);
	}

	public void writeToFile(String outputFileName) {
		STAXWriterHelper.writeToFile(objectContainer, outputFileName);
	}

	public Flowers getObjectContainer() {
		return objectContainer;
	}
}