package com.epam.rd.java.basic.task8.model;

public class VisualParameters {
    private String stemColour, leafColour;
    private AveLenFlowers aveLenFlowers;

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, AveLenFlowers aveLenFlowers) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlowers = aveLenFlowers;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public AveLenFlowers getAveLenFlowers() {
        return aveLenFlowers;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlowers(AveLenFlowers aveLenFlowers) {
        this.aveLenFlowers = aveLenFlowers;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", " + aveLenFlowers +
                '}';
    }
}
