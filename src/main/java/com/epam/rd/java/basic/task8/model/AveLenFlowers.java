package com.epam.rd.java.basic.task8.model;

public class AveLenFlowers {
    private int aveLenFlower;
    private String measure;

    public AveLenFlowers() {}

    public AveLenFlowers(int aveLenFlower, String measure) {
        this.aveLenFlower = aveLenFlower;
        this.measure = measure;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public String getMeasure() {
        return measure;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "AveLenFlowers{" +
                "aveLenFlower=" + aveLenFlower +
                ", measure='" + measure + '\'' +
                '}';
    }
}
