package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.util.Comparator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		DOMController domController = new DOMController(xmlFileName);
		domController.validate();
		domController.parse();
		domController.sortObjectContainer(Comparator.comparing(Flower::getName));
		String outputXmlFile = "output.dom.xml";
		domController.writeToFile(outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		SAXController saxController = new SAXController(xmlFileName);

		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(new File(xmlFileName.replaceFirst(".xml", ".xsd")));

		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setSchema(schema);

		SAXParser parser = factory.newSAXParser();
		parser.parse(new File(xmlFileName), saxController);
		saxController.sortObjectContainer(Comparator.comparing(Flower::getName));
		outputXmlFile = "output.sax.xml";
		saxController.writeToFile(outputXmlFile);


		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		STAXController staxController = new STAXController(xmlFileName);
		staxController.validate();
		staxController.parse();
		staxController.sortObjectContainer(Comparator.comparing(Flower::getName));
		outputXmlFile = "output.stax.xml";
		staxController.writeToFile(outputXmlFile);
	}

}
