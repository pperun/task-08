package com.epam.rd.java.basic.task8.model;


public class Flower {
    private String name, soil, origin, multiplying;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    public Flower() {
    }

    public Flower(String name, String soil, String origin, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    private String prettyPrint(String text) {
        StringBuilder sb = new StringBuilder(text);
        int openBracesCount = 0;
        for(int i = 0; i < sb.length(); i++) {
            if(sb.charAt(i) == '{') {
                openBracesCount++;
                sb.insert(i, ":\n").insert(i + 2, "\t".repeat(openBracesCount));
                i += (2 + openBracesCount);
            }
            else if(sb.charAt(i) == '}') {
                openBracesCount--;
            }
            else if(sb.charAt(i) == ',') {
                sb.insert(i, "\n").insert(i + 1, "\t".repeat(openBracesCount));
                i += (1 + openBracesCount);
            }
        }

        return sb.toString()
                .replace("=", ": ")
                .replace("{", "")
                .replace("}", "")
                .replace(", ", "");
    }

    @Override
    public String toString() {
        return prettyPrint("\nFlower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", multiplying='" + multiplying + '\'' +
                ", " + visualParameters +
                ", " + growingTips +
                '}');
    }
}

