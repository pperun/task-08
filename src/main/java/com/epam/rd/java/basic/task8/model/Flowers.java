package com.epam.rd.java.basic.task8.model;

import java.util.List;

public class Flowers {
    private List<Flower> flowers;

    public Flowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "\nflowers=" + flowers +
                "\n}";
    }
}
