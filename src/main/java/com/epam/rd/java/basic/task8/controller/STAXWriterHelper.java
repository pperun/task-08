package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;

import static com.epam.rd.java.basic.task8.controller.TagNames.*;
import static com.epam.rd.java.basic.task8.controller.AttributeNames.*;

public class STAXWriterHelper {

    public static void writeToFile(Flowers objectContainer, String outputFileName) {
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xsw = null;
        String indent = "\t";

        try {
            xsw = xof.createXMLStreamWriter(new FileWriter(outputFileName));

            xsw.writeStartDocument();
            xsw.writeCharacters("\n");
            xsw.writeStartElement(FLOWERS);
            xsw.writeAttribute(RootElementAttributes.NAME_SPACE, RootElementAttributes.XMLNameSpace());
            xsw.writeAttribute(RootElementAttributes.PREFIX, RootElementAttributes.prefix());
            xsw.writeAttribute(RootElementAttributes.SCHEMA_LOCATION, RootElementAttributes.schemaLocation());

            for (Flower flower : objectContainer.getFlowers()) {
                xsw.writeCharacters("\n" + indent);
                xsw.writeStartElement(FLOWER);
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeStartElement(NAME);
                xsw.writeCharacters(flower.getName());
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeStartElement(SOIL);
                xsw.writeCharacters(flower.getSoil());
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeStartElement(ORIGIN);
                xsw.writeCharacters(flower.getOrigin());
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeStartElement(VISUAL_PARAMETERS);
                xsw.writeCharacters("\n" + indent.repeat(3));

                xsw.writeStartElement(STEM_COLOUR);
                xsw.writeCharacters(flower.getVisualParameters().getStemColour());
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(3));

                xsw.writeStartElement(LEAF_COLOUR);
                xsw.writeCharacters(flower.getVisualParameters().getLeafColour());
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(3));

                xsw.writeStartElement(AVE_LEN_FLOWER);
                xsw.writeAttribute(MEASURE, flower.getVisualParameters().getAveLenFlowers().getMeasure());
                xsw.writeCharacters(Integer.toString(flower.getVisualParameters().getAveLenFlowers().getAveLenFlower()));
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeStartElement(GROWING_TIPS);
                xsw.writeCharacters("\n" + indent.repeat(3));


                xsw.writeStartElement(TEMPERATURE);
                xsw.writeAttribute(MEASURE, flower.getGrowingTips().getTemperature().getMeasure());
                xsw.writeCharacters(Integer.toString(flower.getGrowingTips().getTemperature().getTemperature()));
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(3));

                xsw.writeEmptyElement(LIGHTING);
                xsw.writeAttribute(LIGHT_REQUIRING, flower.getGrowingTips().getLighting().getLightRequiring());
                xsw.writeCharacters("\n" + indent.repeat(3));

                xsw.writeStartElement(WATERING);
                xsw.writeAttribute(MEASURE, flower.getGrowingTips().getWatering().getMeasure());
                xsw.writeCharacters(Integer.toString(flower.getGrowingTips().getWatering().getWatering()));
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent.repeat(2));

                xsw.writeStartElement(MULTIPLYING);
                xsw.writeCharacters(flower.getMultiplying());
                xsw.writeEndElement();
                xsw.writeCharacters("\n" + indent);

                xsw.writeEndElement();
                xsw.writeCharacters("\n");
            }

            xsw.writeEndElement();
            xsw.writeEndDocument();
            xsw.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (xsw != null) {
                    xsw.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
