package com.epam.rd.java.basic.task8.controller;

public class TagNames {
    public static final String FLOWERS = "flowers",
            FLOWER = "flower",
            NAME = "name",
            SOIL = "soil",
            ORIGIN = "origin",
            VISUAL_PARAMETERS = "visualParameters",
            STEM_COLOUR = "stemColour",
            LEAF_COLOUR = "leafColour",
            AVE_LEN_FLOWER = "aveLenFlower",
            GROWING_TIPS = "growingTips",
            TEMPERATURE = "tempreture",
            LIGHTING = "lighting",
            WATERING = "watering",
            MULTIPLYING = "multiplying";
}
