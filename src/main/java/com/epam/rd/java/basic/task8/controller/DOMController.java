package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.epam.rd.java.basic.task8.controller.AttributeNames.*;
import static com.epam.rd.java.basic.task8.controller.TagNames.*;


/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private final Document inputDocument;
	private Flowers objectContainer;

	public DOMController(String xmlFileName) throws ParserConfigurationException, IOException, SAXException {
		this.xmlFileName = xmlFileName;
		this.inputDocument = buildInputDocument();
	}

	private Document buildInputDocument() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		return dbf.newDocumentBuilder().parse(new File(xmlFileName));
	}

	public void validate() {
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			Schema schema = factory.newSchema(new File(xmlFileName.replaceFirst(".xml", ".xsd")));

			Validator validator = schema.newValidator();
			validator.validate(new DOMSource(inputDocument));
		}
		catch (IOException | SAXException e) {
			e.printStackTrace();
		}

	}

	private Document buildOutputDocument() throws ParserConfigurationException {
		Document outputDocument = null;
		try {
			outputDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Element root = outputDocument.createElement(FLOWERS);
			outputDocument.appendChild(root);
			root.setAttribute(RootElementAttributes.NAME_SPACE, RootElementAttributes.XMLNameSpace());
			root.setAttribute(RootElementAttributes.PREFIX, RootElementAttributes.prefix());
			root.setAttribute(RootElementAttributes.SCHEMA_LOCATION, RootElementAttributes.schemaLocation());

			for(Flower f : objectContainer.getFlowers()) {
				Element flower = outputDocument.createElement(FLOWER);
				root.appendChild(flower);

				Element name = outputDocument.createElement(NAME);
				name.appendChild(outputDocument.createTextNode(f.getName()));

				Element soil = outputDocument.createElement(SOIL);
				soil.appendChild(outputDocument.createTextNode(f.getSoil()));

				Element origin = outputDocument.createElement(ORIGIN);
				origin.appendChild(outputDocument.createTextNode(f.getOrigin()));

				Element visualParameters = outputDocument.createElement(VISUAL_PARAMETERS);

				Element stemColour = outputDocument.createElement(STEM_COLOUR);
				stemColour.appendChild(outputDocument.createTextNode(f.getVisualParameters().getStemColour()));

				Element leafColour = outputDocument.createElement(LEAF_COLOUR);
				leafColour.appendChild(outputDocument.createTextNode(f.getVisualParameters().getLeafColour()));

				Element aveLenFlower = outputDocument.createElement(AVE_LEN_FLOWER);
				aveLenFlower.setAttribute(MEASURE, f.getVisualParameters().getAveLenFlowers().getMeasure());
				aveLenFlower.appendChild(outputDocument.createTextNode(Integer.toString(f.getVisualParameters().getAveLenFlowers().getAveLenFlower())));

				visualParameters.appendChild(stemColour);
				visualParameters.appendChild(leafColour);
				visualParameters.appendChild(aveLenFlower);

				Element growingTips = outputDocument.createElement(GROWING_TIPS);

				Element temperature = outputDocument.createElement(TEMPERATURE);
				temperature.setAttribute(MEASURE, f.getGrowingTips().getTemperature().getMeasure());
				temperature.appendChild(outputDocument.createTextNode(Integer.toString(f.getGrowingTips().getTemperature().getTemperature())));

				Element lighting = outputDocument.createElement(LIGHTING);
				lighting.setAttribute(LIGHT_REQUIRING, f.getGrowingTips().getLighting().getLightRequiring());

				Element watering = outputDocument.createElement(WATERING);
				watering.setAttribute(MEASURE, f.getGrowingTips().getWatering().getMeasure());
				watering.appendChild(outputDocument.createTextNode(Integer.toString(f.getGrowingTips().getWatering().getWatering())));

				growingTips.appendChild(temperature);
				growingTips.appendChild(lighting);
				growingTips.appendChild(watering);

				Element multiplying = outputDocument.createElement(MULTIPLYING);
				multiplying.appendChild(outputDocument.createTextNode(f.getMultiplying()));

				flower.appendChild(name);
				flower.appendChild(soil);
				flower.appendChild(origin);
				flower.appendChild(visualParameters);
				flower.appendChild(growingTips);
				flower.appendChild(multiplying);
			}
		}
		catch (ParserConfigurationException e) {
			e.printStackTrace();
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		}
		return outputDocument;
	}

	private boolean isNodeElement(Node node) {
		return node.getNodeType() == Node.ELEMENT_NODE;
	}

	public void parse() {
		List<Flower> flowersList = new ArrayList<>();
		NodeList rootNodeList = inputDocument.getDocumentElement().getElementsByTagName(FLOWER);

		for(int i = 0; i < rootNodeList.getLength(); i++) {
			if(!isNodeElement(rootNodeList.item(i))) {
				continue;
			}

			NodeList flowerNodeList = rootNodeList.item(i).getChildNodes();
			String name = "", soil = "", origin = "", multiplying = "";
			VisualParameters visualParameters = null;
			GrowingTips growingTips = null;

			for(int j = 0; j < flowerNodeList.getLength(); j++) {
				if(!isNodeElement(flowerNodeList.item(j))) {
					continue;
				}

				Node flowerNode = flowerNodeList.item(j);
				switch(flowerNode.getNodeName()) {
					case NAME:
						name = flowerNode.getTextContent();
						break;
					case SOIL:
						soil = flowerNode.getTextContent();
						break;
					case ORIGIN:
						origin = flowerNode.getTextContent();
						break;
					case VISUAL_PARAMETERS:
						NodeList visualParametersChildList = flowerNode.getChildNodes();
						String stemColour = "", leafColour = "";
						AveLenFlowers aveLenFlowers = null;

						for(int k = 0; k < visualParametersChildList.getLength(); k++) {
							if(!isNodeElement(visualParametersChildList.item(k))) {
								continue;
							}

							Element visualParameter = (Element) visualParametersChildList.item(k);
							switch(visualParameter.getNodeName()) {
								case STEM_COLOUR:
									stemColour = visualParameter.getTextContent();
									break;
								case LEAF_COLOUR:
									leafColour = visualParameter.getTextContent();
									break;
								case AVE_LEN_FLOWER:
									aveLenFlowers = new AveLenFlowers(
											Integer.parseInt(visualParameter.getTextContent()),
											visualParameter.getAttribute(MEASURE)
									);
									break;
							}
						}

						visualParameters = new VisualParameters(stemColour, leafColour, aveLenFlowers);
						break;
					case GROWING_TIPS:
						NodeList growingTipsChildList = flowerNode.getChildNodes();
						Temperature temperature = null;
						Lighting lighting = null;
						Watering watering = null;

						for(int k = 0; k < growingTipsChildList.getLength(); k++) {
							if(!isNodeElement(growingTipsChildList.item(k))) {
								continue;
							}

							Element growingTip = (Element) growingTipsChildList.item(k);
							switch(growingTip.getNodeName()) {
								case TEMPERATURE:
									temperature = new Temperature(
											Integer.parseInt(growingTip.getTextContent()),
											growingTip.getAttribute(MEASURE)
									);
									break;
								case LIGHTING:
									lighting = new Lighting(growingTip.getAttribute(LIGHT_REQUIRING));
									break;
								case WATERING:
									watering = new Watering(
											Integer.parseInt(growingTip.getTextContent()),
											growingTip.getAttribute(MEASURE)
									);
									break;
							}
						}

						growingTips = new GrowingTips(temperature, lighting, watering);
						break;
					case MULTIPLYING:
						multiplying = flowerNode.getTextContent();
				}
			}
			flowersList.add(new Flower(name, soil, origin, visualParameters, growingTips, multiplying));
		}

		objectContainer = new Flowers(flowersList);
	}

	public void sortObjectContainer(Comparator<Flower> flowerComparator) {
		objectContainer.getFlowers().sort(flowerComparator);
	}

	public void writeToFile(String outputFileName) throws TransformerException, IOException, ParserConfigurationException {
		try {
			File outputFile = new File(outputFileName);
			outputFile.createNewFile();
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(new DOMSource(buildOutputDocument()), new StreamResult(outputFile));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Flowers getObjectContainer() {
		return objectContainer;
	}

	@Override
	public String toString() {
		return "DOMController{" +
				"xmlFileName='" + xmlFileName + '\'' +
				", objectContainer=" + objectContainer +
				'}';
	}
}
