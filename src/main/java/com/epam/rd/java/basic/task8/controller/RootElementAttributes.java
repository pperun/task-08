package com.epam.rd.java.basic.task8.controller;

import java.util.HashMap;
import java.util.Map;

public class RootElementAttributes {
    public static final String NAME_SPACE = "xmlns", PREFIX = "xmlns:xsi", SCHEMA_LOCATION = "xsi:schemaLocation";

    private static final Map<String, String> rootElementAttributes = new HashMap<>();
    static {
        rootElementAttributes.put(NAME_SPACE, "http://www.nure.ua");
        rootElementAttributes.put(PREFIX, "http://www.w3.org/2001/XMLSchema-instance");
        rootElementAttributes.put(SCHEMA_LOCATION, "http://www.nure.ua input.xsd ");
    }

    public static String XMLNameSpace() {
        return rootElementAttributes.get(NAME_SPACE);
    }

    public static String prefix() {
        return rootElementAttributes.get(PREFIX);
    }

    public static String schemaLocation() {
        return rootElementAttributes.get(SCHEMA_LOCATION);
    }
}
