package com.epam.rd.java.basic.task8.model;

public class Watering {
    private int watering;
    private String measure;

    public Watering(int watering, String measure) {
        this.watering = watering;
        this.measure = measure;
    }

    public int getWatering() {
        return watering;
    }

    public String getMeasure() {
        return measure;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "Watering{" +
                "watering=" + watering +
                ", measure='" + measure + '\'' +
                '}';
    }
}
