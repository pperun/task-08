package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.epam.rd.java.basic.task8.controller.TagNames.*;
import static com.epam.rd.java.basic.task8.controller.AttributeNames.*;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;

	private List<Flower> flowerList;
	private Flowers objectContainer;
	private Flower currentFlower;

	private StringBuilder elementValue;
	private Attributes elementAttributes;



	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public void startDocument() {
		elementValue = new StringBuilder();
	}

	@Override
	public void endDocument() {
		objectContainer = new Flowers(flowerList);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		switch(qName) {
			case FLOWERS:
				flowerList = new ArrayList<>();
				break;
			case FLOWER:
				currentFlower = new Flower();
				flowerList.add(currentFlower);
				break;
			case VISUAL_PARAMETERS:
				currentFlower.setVisualParameters(new VisualParameters());
				break;
			case GROWING_TIPS:
				currentFlower.setGrowingTips(new GrowingTips());
				break;
			case AVE_LEN_FLOWER:
			case TEMPERATURE:
			case LIGHTING:
			case WATERING:
				elementAttributes = attributes;
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		try {
			switch(qName) {
				case NAME:
					currentFlower.setName(elementValue.toString());
					break;
				case SOIL:
					currentFlower.setSoil(elementValue.toString());
					break;
				case ORIGIN:
					currentFlower.setOrigin(elementValue.toString());
					break;
				case STEM_COLOUR:
					currentFlower.getVisualParameters().setStemColour(elementValue.toString());
					break;
				case LEAF_COLOUR:
					currentFlower.getVisualParameters().setLeafColour(elementValue.toString());
					break;
				case AVE_LEN_FLOWER:
					currentFlower.getVisualParameters().setAveLenFlowers(
							new AveLenFlowers(
									Integer.parseInt(elementValue.toString()),
									elementAttributes.getValue(MEASURE))
					);
					break;
				case TEMPERATURE:
					currentFlower.getGrowingTips().setTemperature(
							new Temperature(
									Integer.parseInt(elementValue.toString()),
									elementAttributes.getValue(MEASURE))
					);
					break;
				case LIGHTING:
					currentFlower.getGrowingTips().setLighting(new Lighting(elementAttributes.getValue(LIGHT_REQUIRING)));
					break;
				case WATERING:
					currentFlower.getGrowingTips().setWatering(
							new Watering(
									Integer.parseInt(elementValue.toString()),
									elementAttributes.getValue(MEASURE))
					);
					break;
				case MULTIPLYING:
					currentFlower.setMultiplying(elementValue.toString());
					break;
			}
		}
		catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		elementValue.delete(0, elementValue.length());
		elementValue.append(ch, start, length);
	}

	public void sortObjectContainer(Comparator<Flower> flowerComparator) {
		objectContainer.getFlowers().sort(flowerComparator);
	}

	public void writeToFile(String outputFileName) {
		STAXWriterHelper.writeToFile(objectContainer, outputFileName);
	}

	public Flowers getObjectContainer() {
		return objectContainer;
	}
}