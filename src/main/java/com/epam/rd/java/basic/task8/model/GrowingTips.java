package com.epam.rd.java.basic.task8.model;

public class GrowingTips {
    private Temperature temperature;
    private Lighting lighting;
    private Watering watering;

    public GrowingTips() {
    }

    public GrowingTips(Temperature temperature, Lighting lighting, Watering watering) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                temperature +
                ", " + lighting +
                ", " + watering +
                '}';
    }
}
